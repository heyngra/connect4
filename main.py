import os
import random
import json
from colorama import Fore


def print_header():
    print(Fore.GREEN)
    print('------------------------------')
    print('        Connect 4 Game')
    print('------------------------------')
    print(Fore.WHITE)


def check_which_row(board, column):
    for row in range(len(board) - 1, -1, -1):
        if board[row][column] is None:
            return row
    return None


def play(board, symbol):
    if symbol == 'X':
        try:
            column = int(input('Which column to play: '))
        except ValueError:
            return False
    else:
        column = random.randint(1, 7)
        global computer_play
        computer_play = column

    column -= 1
    if column < 0 or column > len(board[0]):
        return False

    row = check_which_row(board, column)
    if row is not None:
        board[row][column] = symbol
        return True
    else:
        return False


def announce_turn(player):
    print(Fore.LIGHTYELLOW_EX + f'It\'s {player}\'s turn.' + Fore.WHITE)


def get_winning_sequences(board):
    # Declaring winning sequences
    sequences = []

    # Adding rows to winning sequences
    for col_idx in range(0, 4):
        for row_idx in range(0, 6):
            row = [
                board[row_idx][col_idx],
                board[row_idx][1 + col_idx],
                board[row_idx][2 + col_idx],
                board[row_idx][3 + col_idx]
            ]
            sequences.append(row)

    # Adding columns to winning sequences
    for col_idx in range(0, 7):
        for row_idx in range(0, 3):
            col = [
                board[row_idx][col_idx],
                board[1 + row_idx][col_idx],
                board[2 + row_idx][col_idx],
                board[3 + row_idx][col_idx]
            ]
            sequences.append(col)

    # Adding diagonals to winning sequences
    for col_idx in range(0, 4):
        for row_idx in range(0, 3):
            dia = [
                [board[row_idx][col_idx], board[1 + row_idx][1 + col_idx], board[2 + row_idx][2 + col_idx],
                 board[3 + row_idx][3 + col_idx]],
                [board[3 + row_idx][col_idx], board[2 + row_idx][1 + col_idx], board[1 + row_idx][2 + col_idx],
                 board[row_idx][3 + col_idx]]
            ]
            sequences.extend(dia)

    return sequences


def find_winner(board):
    sequences = get_winning_sequences(board)
    # Checking every cell in sequences if winning sequence is on the board
    for cells in sequences:
        symbol = cells[0]
        if symbol and all(symbol == cell for cell in cells):
            return True

    return False


def show_board(board):
    # Showing the board
    for row in board:
        print('| ', end='')
        for cell in row:
            symbol = cell if cell is not None else '_'
            print(symbol, end=' | ')
        print()


def load_leaders():
    directory = os.path.dirname(__file__)
    filename = os.path.join(directory, 'leaderboard.json')
    if not os.path.exists(filename):
        return {}

    with open(filename, 'r', encoding='utf-8') as fin:
        return json.load(fin)


def record_win(winner_name):
    leaders = load_leaders()

    if winner_name in leaders:
        leaders[winner_name] += 1
    else:
        leaders[winner_name] = 1

    directory = os.path.dirname(__file__)
    filename = os.path.join(directory, 'leaderboard.json')

    with open(filename, 'w', encoding='utf-8') as fout:
        json.dump(leaders, fout)


def print_leaderboard():
    leaders = load_leaders()

    sorted_names = list(leaders.items())
    sorted_names.sort(key=lambda l: l[1], reverse=True)

    print()
    print('Leaders:')
    if not leaders:
        print(Fore.LIGHTRED_EX + 'There are no leaders yet!' + Fore.WHITE)
    else:
        for name, wins in sorted_names[0:5]:
            print(Fore.GREEN + f'{name} -- {wins:,}' + Fore.WHITE)

    print()
    print('------------------------------')


def main():
    print_header()
    print_leaderboard()
    board = [
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None]
    ]
    player_1 = input('What is your name? ')
    players = [player_1, 'Computer']
    symbols = ['X', 'O']
    active_player_index = 0
    player = players[active_player_index]
    while not find_winner(board):
        player = players[active_player_index]
        symbol = symbols[active_player_index]
        show_board(board)
        announce_turn(player)
        if not play(board, symbol):
            print('This is not an option, try again.')
            continue
        if player == 'Computer':
            print(Fore.GREEN + f'Computer played {computer_play}' + Fore.WHITE)
        active_player_index = (active_player_index + 1) % 2

    msg = Fore.LIGHTRED_EX + f'Game over! {player} has won!' + Fore.WHITE
    print(msg)
    record_win(player)
    print('The winning board is: ')
    show_board(board)


if __name__ == '__main__':
    main()
